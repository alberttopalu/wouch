<?php

use Illuminate\Database\Seeder;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Comment::truncate();

        \App\Comment::create([
            "id" => 1,
            "post_id" => (int)1,
            "content" => "comment 1",
            "commentator_id" => (int)1,
            'created_at' => '2020-07-23 16:36:50',
         ]);

        \App\Comment::create([
            "id" => 2,
            "post_id" => (int)1,
            "content" => "comment 2",
            "commentator_id" => (int)1,
            'created_at' => '2020-07-18 16:36:50',
         ]);

        \App\Comment::create([
            "id" => 3,
            "post_id" => (int)2,
            "content" => "comment 3",
            "commentator_id" => (int)2,
            'created_at' => '2020-07-19 16:36:50',
         ]);

        \App\Comment::create([
            "id" => 4,
            "post_id" => (int)2,
            "content" => "comment 4",
            "commentator_id" => (int)2,
            'created_at' => '2020-07-20 16:36:50',
         ]);

        \App\Comment::create([
            "id" => 5,
            "post_id" => (int)3,
            "content" => "comment 5",
            "commentator_id" => (int)3,
            'created_at' => '2020-07-23 16:36:50',
         ]);

        \App\Comment::create([
            "id" => 6,
            "post_id" => (int)3,
            "content" => "comment 7",
            "commentator_id" => (int)3,
            'created_at' => '2020-07-24 16:36:50',
         ]);

        \App\Comment::create([
            "id" => 7,
            "post_id" => (int)3,
            "content" => "comment 8",
            "commentator_id" => (int)3,
            'deleted_at' => '2020-07-14 16:36:50',
        ]);



        if (config("database.default") == "pgsql") {

            \Illuminate\Support\Facades\DB::statement("SELECT setval('\"public\".\"comments_id_seq\"', 100, true)");
        }

    }
}
