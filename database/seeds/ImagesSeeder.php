<?php

use Illuminate\Database\Seeder;

class ImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Images::truncate();

        \App\Images::create([
            "id" => 1,
            "url" => "http://lorempixel.com/output/food-q-c-640-480-2.jpg",
            'created_at' => '2020-07-18 16:36:50',
            'updated_at' => '2020-07-18 16:36:50',
        ]);

        \App\Images::create([
            "id" => 2,
            "url" => "http://lorempixel.com/output/food-q-c-640-480-3.jpg",
            'created_at' => '2020-07-18 16:36:50',
            'updated_at' => '2020-07-18 16:36:50',
        ]);

        \App\Images::create([
            "id" => 3,
            "url" => "http://lorempixel.com/output/food-q-c-640-480-4.jpg",
            'created_at' => '2020-07-18 16:36:50',
            'updated_at' => '2020-07-18 16:36:50',
        ]);


        if (config("database.default") == "pgsql") {

            \Illuminate\Support\Facades\DB::statement("SELECT setval('\"public\".\"images_id_seq\"', 100, true)");
        }

    }
}
