<?php

use Illuminate\Database\Seeder;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \App\Post::truncate();

        \App\Post::create([
            "id" => 1,
            "author_id" => 1,
            "content" => "I love shashlyndos",
            "image_id" =>(int)1,
            'created_at' => '2020-07-23 16:36:50',

        ]);



        \App\Post::create([
            "id" => 2,
            "author_id" => 2,
            "content" => "I love shashlyndos",
            "image_id" =>(int)2,
            'created_at' => '2020-07-24 16:36:50',
         ]);




        \App\Post::create([
            "id" => 3,
            "author_id" => 3,
            "content" => "Test",
            "image_id" =>(int)3,
            'created_at' => '2020-07-25 16:36:50',
         ]);

        \App\Post::create([
            "id" => 4,
            "author_id" => 1,
            "content" => "deleted post",
            "image_id" =>(int)1,
            "deleted_at" => '2020-07-15 16:36:50',
        ]);

        \App\Post::create([
            "id" => 5,
            "author_id" => 2,
            "content" => "deleted post",
            "image_id" =>(int)2,
            "deleted_at" => '2020-07-10 16:36:50',
         ]);



        if (config("database.default") == "pgsql") {

            \Illuminate\Support\Facades\DB::statement("SELECT setval('\"public\".\"posts_id_seq\"', 100, true)");
        }

    }
}
