<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

        public function run()
    {
        \App\User::truncate();

        \App\User::create([
            "id" => 1,
            "name" => "Petro Ostapovich",
            "password" => "1111",
            "email" => "petro@gmail.com",
            'created_at' => '2020-07-18 16:36:50',
            'updated_at' => '2020-07-18 16:36:50',
        ]);

        \App\User::create([
            "id" => 2,
            "name" => "Ivan",
            "password" => "2222",
            "email" => "ivan@gmail.com",
            'created_at' => '2020-07-18 16:36:50',
            'updated_at' => '2020-07-18 16:36:50',
        ]);

        \App\User::create([
            "id" => 3,
            "name" => "mikle",
            "password" => "3333",
            "email" => "mikle@gmail.com",
            'created_at' => '2020-07-18 16:36:50',
            'updated_at' => '2020-07-18 16:36:50',
        ]);


        if(config("database.default") == "pgsql"){

            \Illuminate\Support\Facades\DB::statement("SELECT setval('\"public\".\"users_id_seq\"', 100, true)");
        }

    }

}
