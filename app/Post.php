<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table= 'posts';

    protected $primaryKey = "id";

    protected $fillable = [
        'author_id',
        'image_id',
        'content',
        'deleted_at',
        'created_at'
    ];

    public function image()
    {
        return $this->hasOne(Images::class, "id", "image_id")->select('id','url');
    }

    public function comment()
    {
        return $this->hasMany(Comment::class, "post_id", "id");
    }

}
