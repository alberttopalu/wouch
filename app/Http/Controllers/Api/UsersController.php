<?php

namespace App\Http\Controllers\Api;

use App\Comment;
 use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{

    //6. Создать API метод (роут) для получения всех активных пользователей
    //6.2*
    public function index(){

        $get_users = User::select('id','name')->with('post.image','post.comment')
            ->where('active', true)
             ->get()->toArray();

        $users = [];
        foreach ($get_users as $user) {
            foreach ($user['post'] as $post)
            $users[] = [
                "id" => $user["id"],
                "name" => $user["name"],
                "post" => [
                    'id'=> $post['id'],
                    'content'=> $post['content'],
                    'created_at_ts'=> strtotime($post['created_at']),
                    'image_url'=> $post['image']['url'],
                    'count_of_comments' => count($post['comment'])
                ],
            ];
        }

     return  ['data'=> $users];
    }

    //7. Создать роут, который принимает параметр user_id
    //7.1
    //7.2*
    //7.2.1***.


    public function getUser(Request $request ){

        $get_comments = Comment::with('post','commentator')
            ->where('commentator_id',$request->user_id)
            ->orderBy('created_at', 'DESC')
            ->get()->toArray();

        $comments=[];
        foreach ($get_comments as $item){

            if ($item['post']['image_id'] !== null ){
                $comments[] = [
                    'id' => $item['id'],
                    'post_id' => $item['post_id'],
                    'commentator_id' => $item['commentator_id'],
                    'content' => $item['content'],
                    'deleted_at' => $item['deleted_at'],
                    'created_at' => $item['created_at'],
                    'post' => [
                      'image_id' =>  $item['post']['image_id'],
                      'commentator' =>  ( ($item['commentator']['active'] == true) ? $item['commentator']['name'] : null )
                    ],
                ];
            }
        }

     return   $comments;

    }
}
