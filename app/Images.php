<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table= 'images';

    protected $primaryKey = "id";

    protected $fillable = [
        'url',
    ];
}
