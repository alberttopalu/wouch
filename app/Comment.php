<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table= 'comments';

    protected $primaryKey = "id";

    protected $fillable = [
        'post_id',
        'commentator_id',
        'content',
        'deleted_at'
    ];

    public function image()
    {
        return $this->hasOne(Images::class, "id", "image_id")->select('id','url');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, "post_id", "id");
    }

    public function commentator()
    {
        return $this->hasOne(User::class, "id", "commentator_id")->select('id','active','name');
    }


}
